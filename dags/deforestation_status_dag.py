import logging
from google.cloud import bigquery
from datetime import datetime, timedelta
from airflow.operators.python import PythonOperator
from datetime import datetime, timedelta
from airflow import DAG

# DAG definition
default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=5),
}

dag = DAG(
    'deforestation_row_count',
    default_args=default_args,
    description='Deforestation Status for Farms',
    schedule_interval=timedelta(days=1),
    start_date=datetime(2021, 1, 1),
    catchup=False,
)

def run_query(query, **kwargs):
    """Executes a query in BigQuery."""
    try:
        client = bigquery.Client()
        query_job = client.query(query)
        results = query_job.result()  # Waits for the query to finish
        
        for row in results:
            logging.info(f"{row}")  # Log each row of results

    except Exception as err:
        logging.error(f"Query execution failed: {err}")
        raise


# Deforestation status query
query = '''
SELECT DeforestationStatus, count (distinct FarmID) quantity
FROM `data-engineering-test-task.deforestation.deforestation_status`
group by 1;
'''

execute_query = PythonOperator(
    task_id='run_bigquery_query',
    python_callable=run_query,
    op_kwargs={'query': query}, 
    dag=dag,
)

execute_query